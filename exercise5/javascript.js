let myInterval = setInterval(myTimer, 1000);
const clock = document.getElementById('clock');
const myButton = document.getElementById('myButton');


function myTimer() {
  const date = new Date();
  document.getElementById("clock").innerHTML = date.toLocaleTimeString();
}

let buttonState = 'run'

myButton.onclick = ()=> {
    if(buttonState === 'run'){
        clearInterval(myInterval);
        buttonState = 'stop';
    }else {
        myInterval = setInterval(myTimer, 1000);
        buttonState = 'run';
    }
    
}